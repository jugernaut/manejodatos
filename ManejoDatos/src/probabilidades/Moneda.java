package probabilidades;
import java.util.Random;
 
/**
 * Clase que muestra como simular probabilidades al
 * generar valores aleatorios
 * @author mike  
 */
public class Moneda {
    
    private boolean cara;

    /**
     * Constructor de monedas justas
     * @param cara true = cara, false = cruz
     */
    public Moneda(boolean cara) {
        this.cara = cara;
    }

    /**
     * Getters y Setters
     * @return 
     */
    public boolean getCara() {
        return cara;
    }

    public void setCara(boolean cara) {
        this.cara = cara;
    }
    
    /**
     * Simula el lanzamiento de una moneda justa
     */
    public void lanzamiento(){
        Random rd = new Random(); // generamos un booleano aleatorio
        cara = rd.nextBoolean(); // se guarda el resultado en cara
    }
    
    /**
     * Simula rl lanzamiento de una moneda tramposa haciendo uso de
     * una carga para determinar cual sera el valor que tiene mayor probabilidad
     * de ser generado
     * @param carga de que lado se inclina la moneda 0'0 = cruz 1.0 = cara
     */
    public void lanzamiento(double carga){
        Random rd = new Random(); // generamos un booleano aleatorio
        if(carga < rd.nextDouble()){
            cara = true;
        }else{
            cara = false;
        }
    }
    
    public static void main(String[] args) {
        Moneda moneda = new Moneda(false);
        for(int i=0; i<10; i++){
            moneda.lanzamiento(0.9);
            if(moneda.getCara()){
                System.out.println("Cayo cara :)");
            }else{
                System.out.println("Cayo cruz +");
            }
        }
    }
}
