package manejoArchivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author mike
 */
public class Lector {
    String ruta;
    
    public Lector(String ruta){
        this.ruta = ruta;
    }
    
    public void muestraContenido() throws FileNotFoundException, IOException {
        String cadena;
        File archivo = new File(ruta);
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) {
            System.out.println(cadena);
        }
        b.close();
    }
    
    public static void muestraContenido(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) {
            System.out.println(cadena);
        }
        b.close();
    }

    public static void main(String[] args) throws IOException {
        Lector lector = new Lector("src/manejoArchivos/Clientes.xml");
        lector.muestraContenido();
    }
}
