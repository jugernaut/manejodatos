package conexionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Clase que abre la conexion a alguna base de MySQL
 * @author mike
 */
public class MySQLConexion {
    // Constantes para la conexion
    private final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private final String DATABASE_URL = "jdbc:mysql://localhost:3306/FCiencias";
    private final String USERNAME = "ciencias";
    private final String PASSWORD = "prometeo";
    private final String MAX_POOL = "250";

    // Objeto del paquete Connection que me permite establecer la conexion
    private Connection conexion;
    // Objecto que sirve para almacenar las propiedades de la conexion
    private Properties properties;

    /**
     * Metodo que devuelve el objeto que nos permite realizar consultas a la bd
     * @return 
     */
    public Connection getConexion() {
        return conexion;
    }
    
    /**
     * Este metodo devuelve las propiedades de la conexion
     * @return properties
     */
    private Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
            properties.setProperty("MaxPooledStatements", MAX_POOL);
        }
        return properties;
    }
    
    /**
     * Inicializa el objeto que permite realizar consultas a la
     * base de datos.
     */
    public void conectar() {
        if (conexion == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                conexion = DriverManager.getConnection(DATABASE_URL, getProperties());
                System.out.println("Conexion exitosa");
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Si la conexion ya se establecio, este metodo cierra la conexion y
     * libera la memoria.
     */
    public void desconectar() {
        if (conexion != null) {
            try {
                conexion.close();
                conexion = null;
                System.out.println("ya me deconecte :)");
            } catch (SQLException e) {
                e.printStackTrace(); 
            }
        }
    }
}