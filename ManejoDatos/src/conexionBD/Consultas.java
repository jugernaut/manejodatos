/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexionBD;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author mike
 */
public class Consultas {
    
    private MySQLConexion cone;

    public Consultas() {
        this.cone = new MySQLConexion();
    }
    
    public TableModel tablilla(){
        String sql = "";
        ResultSet rs;
        TableModel tm = null;
        PreparedStatement statement = null;
        cone.conectar();
        try {
            sql = "SELECT * FROM alumnos;";  
            statement = cone.getConexion().prepareStatement(sql);
            rs = statement.executeQuery();
            tm = DbUtils.resultSetToTableModel(rs);            
        } catch (SQLException ex) {
            System.out.println(ex);
        } finally {
            cone.desconectar();
        }
        return tm;
    }
}
