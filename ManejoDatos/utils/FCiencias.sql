CREATE DATABASE FCiencias;

USE FCiencias;

CREATE TABLE carreras (id_carrera INT AUTO_INCREMENT PRIMARY KEY, nombre_carrera CHAR(40) NOT NULL);

CREATE TABLE cursos (id_curso INT AUTO_INCREMENT PRIMARY KEY, nombre_curso CHAR(40));

CREATE TABLE alumnos (cuenta INT AUTO_INCREMENT PRIMARY KEY, nombre VARCHAR(30), id_carrera INT NOT NULL, 
INDEX (id_carrera), FOREIGN KEY (id_carrera) REFERENCES carreras(id_carrera));

CREATE TABLE calificaciones (cuenta INT NOT NULL, id_curso INT NOT NULL, calificacion FLOAT NOT NULL, 
PRIMARY KEY(cuenta,id_curso),
INDEX (cuenta), FOREIGN KEY (cuenta) REFERENCES alumnos(cuenta),
INDEX (id_curso), FOREIGN KEY (id_curso) REFERENCES cursos(id_curso)
);

INSERT INTO carreras (nombre_carrera) VALUES ('actuaria'),('ciencias de la computacion'),('matematicas'),('fisica');

INSERT INTO cursos (nombre_curso) VALUES ('Programacion 1'),('Programacion 2'),('Manejo de Datos'),('Analsis Numerico');

INSERT INTO alumnos (nombre, id_carrera) VALUES ('Mike', 2),('Pedro', 3),('Peter', 4),('Almendra', 1),('Alvaro', 3), ('Canek', 1),('Sonia', 2),('Arturo', 1),('Gisel', 2),('Alfa', 1);

INSERT INTO calificaciones (cuenta, id_curso, calificacion) VALUES (1, 1, 11.0),(1, 2, 11.0),(1, 3, 10.5),(1,4,15.0) ,(2, 1, 8.0),(2, 2, 7.5),(2, 3, 5.0),(2,4,8.0), (3, 1, 9),(3, 2, 9),(3, 3, 9),(3,4,15.9), (4, 1, 6.0),(4, 2, 10.0),(4, 3, 8.0),(4,4,5.5), (5, 1, 10.0),(5, 2, 9.0),(5, 3, 10.0),(5,4,10.0), (6, 1, 7.0),(6, 2, 8.0),(6, 3, 9.5),(6,4,7), (7, 1, 5.0),(7, 2, 5.0),(7, 3, 5.0),(7,4,5.0), (8, 1, 1.0),(8, 2, 0.0),(8, 3, 2.5),(8,4, 4.0), (9, 1, 7.0),(9,4,4.0), (10,2,10.0);
